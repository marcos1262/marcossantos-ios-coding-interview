import SwiftUI

@main
struct iOS_Coding_ExerciseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
