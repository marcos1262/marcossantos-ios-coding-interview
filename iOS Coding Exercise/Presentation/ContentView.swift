import SwiftUI

enum CatFactState {
    case loading
    case error(String)
    case ready([CatFact])
}

struct ContentView: View {
    // MARK: - Properties
    // TODO: Move to ViewModel and keep only 1 reference to it (ObservedObject)
    @State private var state: CatFactState = .loading
    @State private var selectedCatFact: CatFact? = nil
    // TODO: Persist with CoreData/SwiftData
    @State private var favoriteStates: [String: Bool] = [:]

    // MARK: - Body
    var body: some View {
        NavigationStack {
            VStack {
                switch state {
                case .loading:
                    ProgressView()
                case .error(let message):
                    Text(message)
                case .ready(let catFacts):
                    // MARK: - Cat Facts List
                    List(catFacts) { fact in
                        // TODO: Maybe move navigation logic to another entity
                        NavigationLink(destination: CatFactDetailView(catFact: fact)) {
                            HStack(alignment: .top) {
                                Text(fact.text)
                                    .lineLimit(2)
                                Image(systemName: fact.isFavorite ? "heart.fill" : "heart")
                                    .onTapGesture {
                                        updateFavorite(for: fact, in: catFacts)
                                    }
                            }
                        }
                    }
                }
            }
            .onAppear {
                fetchData()
            }
            .navigationTitle("Cat Facts")
        }
    }

    // TODO: Move to ViewModel
    // MARK: - Update Favorite
    func updateFavorite(for fact: CatFact, in catFacts: [CatFact]) {
        if let index = catFacts.firstIndex(where: { $0 == fact }) {
            var newFact = fact
            newFact.isFavorite.toggle()
            self.favoriteStates[fact.id] = newFact.isFavorite
            var catFacts = catFacts
            catFacts[index] = newFact
            self.state = .ready(catFacts)
        }
    }

    // TODO: Move to ViewModel and create Unit Tests for it
    // MARK: - Fetch Data
    func fetchData() {
        state = .loading
        guard let url = URL(string: "https://cat-fact.herokuapp.com/facts") else { return }

        // TODO: Inject dependency
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let data = data {
                do {
                    let catFacts = try JSONDecoder().decode([CatFact].self, from: data)
                    // TODO: Inject dependency
                    DispatchQueue.main.async {
                        let catFacts = catFacts.map {
                            let state = favoriteStates[$0.id] ?? false
                            return CatFact(id: $0.id, text: $0.text, type: $0.type, isFavorite: state)
                        }
                        self.state = .ready(catFacts)
                    }
                } catch {
                    self.state = .error("Error decoding JSON: \(error)")
                }
            } else if let error = error {
                self.state = .error("Error fetching data: \(error)")
            }
        }
        .resume()
    }
}

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
